**Aim : Perform MiTM on Android apps to eavesdrop on client server traffic.**

**Method 1** : Using charles proxy

**Set-up** : Mac os, MiTM with Charles proxy, Android 7.1.2 Mobile device, apk to attack.

**Steps** : 

1. Decompile apk using apktool : https://ilikekillnerds.com/2014/09/how-to-decompile-and-compile-android-apks-on-a-mac-using-apktool/. Make sure to install the latest apktool : https://ibotpeaches.github.io/Apktool/install/

2. Modify AndroidManifest.xml : https://android.jlelse.eu/android-nougat-charlesing-ssl-network-efa0951e66de. (This is needed for Android 7 and above only)

3. Set-up charles proxy on mac machine : http://www.charlesproxy.com/. Look around the internet how to set-up on the machine. e.g. https://community.tealiumiq.com/t5/Tealium-for-Android/Setting-up-Charles-to-Proxy-your-Android-Device/ta-p/5121.

4. Configure Android phone to proxy through your machine e.g. https://community.tealiumiq.com/t5/Tealium-for-Android/Setting-up-Charles-to-Proxy-your-Android-Device/ta-p/5121

5. Install charles proxy ssl cert on your Android Phone. After successful installation :  It should show under Settings > Security > Trusted Credentials > USER

6. Recompile the apk e.g. https://ilikekillnerds.com/2014/09/how-to-decompile-and-compile-android-apks-on-a-mac-using-apktool/

7. Locate the apk generated (For me it was in the decompiled apk folder "dist") (Use command `find . -name \*.apk`)

8. You will have to sign the apk e.g. https://stackoverflow.com/a/24833908/1208682

9. Flash the apk.

10. Intercept the traffic on Charles Proxy on the machine. All req/resp in plaintext. 


Check the MITMed app snapshot here : ![Screenshot](https://gitlab.com/shailesh-mota/mitm-android-apps/blob/master/MITM_Popular_APP.jpg)


**Method 2** : TODO 

DONE !!!

**More Reading** : 

As a Safe-Guard : https://developer.android.com/training/articles/security-ssl.html
As an Attacker : http://blog.opensecurityresearch.com/2013/11/patching-android-application-to-bypass.html ,  https://rammic.github.io/2015/07/28/hiding-secrets-in-android-apps/
